Clean the existing distribute folder:
	rm -r *

Then get the new contents:
rsync -av --exclude-from='../bbOpenREM/stuff/distexclude.txt' ../bbOpenREM/ .

Change settings name:
	mv openrem/openrem/settings.py{,.example}
	mv openrem/openrem/wsgi.py{,.example}

Build:
	python setup.py sdist

